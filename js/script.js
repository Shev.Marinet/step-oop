class Visit  {
    constructor (visitName, date, fullName, comment) {
        this.visitName = visitName;
        this.date = date;
        this.fullName = fullName;
        this.comment = comment;
    }
}
class Therapist extends Visit{
    constructor (visitName, date, fullName, comment, age) {
        super(visitName, date, fullName, comment);
        this.age = age;
        this.name = 'Терапевт';
    }
    get allInfo () {
        return `Цель визита: ${this.visitName}.\nВозраст: ${this.age}.\nЗапланированная дата визита: ${this.date}.\n${this.comment}`;
    }
}
class Dentist extends Visit {
    constructor(visitName, date, fullName, comment, lastVisitDate){
        super(visitName, date, fullName, comment);
        this.lastVisitDate = lastVisitDate;
        this.name = 'Стоматолог';
    }
    get allInfo () {
        return `Цель визита: ${this.visitName}.\nПоследний визит: ${this.lastVisitDate}.\nЗапланированная дата визита: ${this.date}.\n${this.comment}`;
    }
}
class Cardiologist extends Visit {
    constructor(visitName, date, fullName, comment, pressure, bodyMassIndex, diseases, age){
        super(visitName, date, fullName, comment);
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.diseases = diseases;
        this.age = age;
        this.name = 'Кардиолог';
    }
    get allInfo () {
        return `Цель визита: ${this.visitName}.\nОбычное давление: ${this.pressure}.\nИндекс массы тела: ${this.bodyMassIndex}.\nПеренесенные заболевания: ${this.diseases}.\nВозраст: ${this.age}.\nЗапланированная дата визита: ${this.date}.\n${this.comment}`;
    }
}

const common = document.getElementById('common-input');
const modalSection = document.getElementById('modal-section');
const modalOverlay = document.getElementById('modal-overlay');
const formDoctor = document.getElementById('form');
const selectInput = document.getElementById('select-input');

const doctors = document.getElementsByClassName('doctor');
const doctorsInput = document.getElementsByClassName('doctor-input');

let activeDoctor = 0;

let boardWidth = document.getElementById('board').offsetWidth;
const cardWidth = 300;
const cardHeight = 150;

let numberRows = Math.floor(boardWidth/cardWidth);
let mousePositionX = 0;
let mousePositionY = 0;
let counterCards = (localStorage.getItem('counterCards')) ? localStorage.getItem('counterCards') : 0;

if (localStorage.getItem('card')) document.getElementById('board').innerHTML = localStorage.getItem('card');

function positionCards() {
    boardWidth = document.getElementById('board').offsetWidth;
    numberRows = Math.floor(boardWidth/cardWidth);
    if (localStorage.getItem('counterCards')) {
        counterCards = 0;
        document.querySelectorAll('.card').forEach((card) => {
            localStorage.setItem('counterCards', `${++counterCards}`);
            positionCard(card);
        })
    }
}

function setCardColor (card) {
    let x = Math.floor(Math.random()*256);
    let y = Math.floor(Math.random()*256);
    let z = Math.floor(Math.random()*256);
    card.style.backgroundColor = `rgb(${x},${y},${z})`;
}

function positionCard(card) {
    let left = (counterCards > numberRows) ? ((counterCards % numberRows) ? ((counterCards % numberRows)- 1) : numberRows -1 )*cardWidth : (counterCards -1) * cardWidth;
    let top = (counterCards > numberRows) ? Math.floor((counterCards -1)/ numberRows) * cardHeight : 0;
    card.style.left = `${left}px`;
    card.style.top = `${top}px`;
}

function createCard() {
    let div = document.createElement('div');
    div.classList.add('card');
    div.draggable = 'true';
    div.innerHTML = '<i id = "card-close" class = "close-card fas fa-times"></i>';
    document.getElementById('board').appendChild(div);
    setCardColor(div);
    localStorage.setItem('counterCards', `${++counterCards}`);
    positionCard(div);
    return div;
}

function fillCard(card, fullNameObj, doctorObj, allInfoObj) {
    let fragment = document.createDocumentFragment();
    let fullName = document.createElement('p');
    fullName.innerText = fullNameObj;
    fragment.appendChild(fullName);
    let doctor = document.createElement('p');
    doctor.innerText = doctorObj;
    fragment.appendChild(doctor);
    let moreText = document.createElement('a');
    moreText.classList.add('more-text');
    moreText.setAttribute('href','#');
    moreText.innerText = 'Показать больше';
    fragment.appendChild(moreText);
    let allInfo = document.createElement('p');
    allInfo.classList.add('all-info');
    allInfo.innerText = allInfoObj;
    fragment.appendChild(allInfo);
    card.appendChild(fragment);
}

function removeCard (e){
    e.target.parentNode.parentNode.removeChild(e.target.parentNode);
    localStorage.setItem('counterCards', `${--counterCards}`) ;
}

function toggleDocket(place) {
    if (!(place.parentNode.children.length-1)) place.innerText = 'Записей нет';
    else place.innerText = '';
}

positionCards();

document.addEventListener('click', (e) => {
    if (e.target.id === 'card-close') {
        removeCard(e);
        toggleDocket(document.getElementById('board').children[0]);
        localStorage.setItem('card', document.querySelector('.board').innerHTML);
    }
    if (e.target.classList.contains('more-text')) {
        e.target.nextElementSibling.classList.toggle('all-info');
        if (e.target.innerText !== 'Скрыть') {
            e.target.innerText = 'Скрыть';
            e.target.parentNode.style.zIndex = '9';
        } else {
            e.target.innerText = 'Показать больше';
            e.target.parentNode.style.zIndex = '0';
        }
    }
    if(e.target.id === 'button-create') {
        modalSection.hidden = false;
        modalOverlay.hidden = false;
    }
    if(e.target.id === "select-input" || e.target.classList.contains('doctor')) {
        chooseForm(selectInput.selectedIndex);
    }
    if(e.target.id === "close-icon" || e.target.id === "modal-overlay"){
        closeModal();
    }
});
formDoctor.addEventListener('submit', (e) => {
    e.preventDefault();
    let cardFill = createObject();
    let card = createCard();
    fillCard(card, cardFill.fullName, cardFill.name, cardFill.allInfo);
    toggleDocket(document.getElementById('board').children[0]);
    localStorage.setItem('card', document.querySelector('.board').innerHTML);
    closeModal();
});

function chooseForm(index){
    if(index){
        common.hidden = false;
        if(activeDoctor){
            doctorsInput[activeDoctor - 1].hidden = true;
        }
        doctorsInput[index - 1].hidden = false;
        defineRequired(doctorsInput[index - 1], doctorsInput[activeDoctor - 1]);
        activeDoctor = index;
    }
    else {
        if(activeDoctor){
            doctorsInput[activeDoctor - 1].hidden = true;
        }
        common.hidden = true;
        defineRequired(0, doctorsInput[activeDoctor - 1]);
        activeDoctor = index;
    }
}
function defineRequired(requiredDiv, unrequiredDiv) {
    if(requiredDiv) {
        let arrRequired = [...requiredDiv.getElementsByTagName('input')];
        arrRequired.forEach(e => e.required = true);
    }
    if(unrequiredDiv) {
        let arrUnrequired = [...unrequiredDiv.getElementsByTagName('input')];
        arrUnrequired.forEach(e => e.required = false);
    }
}
function closeModal(){
    modalSection.hidden = true;
    modalOverlay.hidden = true;
    selectInput.selectedIndex = "0";
    chooseForm(0);
    formDoctor.reset();
}

function createObject(){
    let visitName = document.getElementById('visit-goal').value;
    let visitDate = document.getElementById('visit-date').value;
    let fullName = document.getElementById('full-name').value;
    let comment = document.getElementById('comment').value;

    if(doctors[activeDoctor].id === 'cardiologist'){
        let pressure = document.getElementById('pressure').value;
        let bodyMassIndex = document.getElementById('body-index').value;
        let diseases = document.getElementById('diseases').value;
        let age = document.getElementById('age-cardio').value;
        return new Cardiologist(visitName, visitDate, fullName, comment, pressure, bodyMassIndex, diseases, age);
    }
    if(doctors[activeDoctor].id === 'dentist'){
        let lastVisit = document.getElementById('last-visit').value;
        return new Dentist(visitName, visitDate, fullName, comment, lastVisit);
    }
    if(doctors[activeDoctor].id === 'therapist'){
        let age = document.getElementById('age-therapist').value;
        return new Therapist(visitName, visitDate, fullName, comment, age);
    }
    return null;
}

window.addEventListener('resize', positionCards);

///////////D&D////////////////

document.onmousedown = function(e) {
    let target = e.target;
    while (target !== document) {
        if (target.classList.contains('card')) {
            target.style.left = `${target.offsetLeft-20}px`;
            target.style.top = `${target.offsetTop-10}px`;
            target.classList.add('card-active');

            mousePositionX = e.clientX - target.offsetLeft;
            mousePositionY = e.clientY - target.offsetTop;
            document.onmousemove = function(e){
                if (e.target.classList.contains('card-active')) {
                    e.target.style.left = `${e.clientX - mousePositionX}px`;
                    e.target.style.top = `${e.clientY - mousePositionY}px`;
                }
            };
            target.onmouseup = function(e) {
                e.target.classList.remove('card-active');
                document.onmousemove = null;
                document.onmouseup = null;
            };
            target.ondragstart = function() {return false};
        }
        target = target.parentNode;
    }
};

